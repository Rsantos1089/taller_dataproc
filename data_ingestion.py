import sys
from pyspark.sql import SparkSession

def main():
    spark = SparkSession.builder \
            .appName('DataIngestion-enero')\
            .config('spark.jars.packages', 'com.google.cloud.spark:spark-bigquery-with-dependencies_2.12:0.15.1-beta') \
            .getOrCreate()

    print("[1] ------- Spark Session Created")

    month = sys.argv[1]
    country = sys.argv[2]

    print("[VAR] ------- Month "+month)

    print("[2] ------- Reading bq tables")

    table = "bigquery-public-data:covid19_ecdc.covid_19_geographic_distribution_worldwide"
    df_covid_latam = spark.read \
                          .format("bigquery") \
                          .option("table", table) \
                          .option("filter", "month = "+ month) \
                          .load()

    print("[3] ------- Transformation ")

    # Seleccionar las columnas a usar, filtrar el país recibido y poner en cache 
    df_covid_subset = df_covid_latam \
                      .select("date", "day", "month", "year", "countries_and_territories","daily_confirmed_cases", "daily_deaths") \
                      .where("countries_and_territories IN ('"+country+"')") \
                      .cache()

    # Escribir en una tabla de BigQuery

    # GCS temporal
    gcs_bucket = 'dataproc-rudy'

    # Es necesario tener un dataset
    bq_dataset = 'medium'

    # BigQuery table que se creará o se sobreescribirá
    bq_table = 'covid_enero'+country.lower()

    print("[4] ------- Saving bq tables")

    df_covid_subset.write \
                   .format("bigquery") \
                   .option("table","{}.{}".format(bq_dataset, bq_table)) \
                   .option("temporaryGcsBucket", gcs_bucket) \
                   .mode('overwrite') \
                   .save()
  
main()